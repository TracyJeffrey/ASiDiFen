/*
Navicat MySQL Data Transfer

Source Server         : aaa
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : lol

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-12-24 20:25:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(30) DEFAULT NULL,
  `admin_pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `admin` VALUES ('2', 'lisi', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `admin` VALUES ('3', 'wangwu', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `admin` VALUES ('4', '111111111111111111111111111111', 'd85247691d13702feab933553f0bb7b0');
INSERT INTO `admin` VALUES ('6', '1111111111111111', '9dcbf642c78137f656ba7c24381ac25b');

-- ----------------------------
-- Table structure for h_jineng
-- ----------------------------
DROP TABLE IF EXISTS `h_jineng`;
CREATE TABLE `h_jineng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `H_id` int(11) DEFAULT NULL COMMENT '英雄id',
  `Skill_name` varchar(255) DEFAULT NULL COMMENT '技能名称',
  `Skill_content` varchar(255) DEFAULT NULL COMMENT '技能内容',
  `Skill_img` varchar(255) DEFAULT NULL COMMENT '技能图标',
  `S_Tips` varchar(255) DEFAULT NULL COMMENT '小贴士',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_jineng
-- ----------------------------
INSERT INTO `h_jineng` VALUES ('1', '1', '穷途末路', '格雷福斯发射一颗火药弹，对一条直线上的敌人造成45/60/75/90/105(+)物理伤害。', 'public/LOL_IMG/nanqiang/jn2.png', '在2秒后，或与地形产生碰撞的0.2秒后，火药弹会爆炸，对附近的所有敌人造成85/115/145/175/205(+)物理伤害。');
INSERT INTO `h_jineng` VALUES ('2', '1', '烟幕弹', '格雷福斯发射一枚烟雾弹，来制造一团持续4秒的黑色烟幕。在黑色烟幕中的敌人无法看到烟幕之外。', 'public/LOL_IMG/nanqiang/jn3.png', '在烟幕弹爆开时造成60/110/160/210/260(+0.6)魔法伤害和15/20/25/30/35%减速效果。');
INSERT INTO `h_jineng` VALUES ('3', '1', '快速拔枪', '格雷福斯向目标区域冲刺，在移动时进行一次快速装弹。格雷福斯获得一层持续4秒的纯爷们效果（可叠加）。如果格雷福斯朝着一名敌方英雄冲刺，则会获得两层纯爷们效果。', 'public/LOL_IMG/nanqiang/jn4.png', '普攻在命中时会使【快速拔枪】的冷却时间缩短0.5秒。在格雷福斯对非小兵单位造成伤害时，纯爷们的持续时间会刷新。');
INSERT INTO `h_jineng` VALUES ('4', '1', '终极爆弹', '格雷福斯射出一枚爆破弹，并因发射时的超强后坐力而被击退。', 'public/LOL_IMG/nanqiang/jn5.png', '对命中的第一个敌人造成250/400/550(+)物理伤害。在命中一名敌方英雄，或者达到最大射程之后，终极爆弹将会分裂，对目标之后锥形范围内的敌人造成200/320/440(+)物理伤害。');
INSERT INTO `h_jineng` VALUES ('5', '2', '诺克萨斯式外交', '泰隆跃向目标并造成65/90/115/140/165(+1.1)物理伤害。如果从近战距离施放，则泰隆不会前跃而是产生暴击，造成%伤害()。', 'public/LOL_IMG/tailong/jn2.png', '当此技能击杀了一个单位时，泰隆会回复生命值并且返还该技能50%的冷却时间。');
INSERT INTO `h_jineng` VALUES ('6', '2', '斩草除根', '泰隆扔出一排刀刃，对命中的目标造成50/65/80/95/110(+)物理伤害并在短暂延迟后返回至泰隆处。', 'public/LOL_IMG/tailong/jn3.png', '在返程时，刀刃会造成70/95/120/145/170(+0.6)额外伤害并使敌人减速40/45/50/55/60%，持续1秒。');
INSERT INTO `h_jineng` VALUES ('7', '2', '刺客之道', '泰隆翻越目标方向上最多码内的距离最近的建筑物或地形。翻越速度与泰隆的移动速度相关联。', 'public/LOL_IMG/tailong/jn4.png', '泰隆无法在秒里重复翻越相同地区的地形。');
INSERT INTO `h_jineng` VALUES ('8', '2', '暗影突袭', '泰隆散出一圈刀刃，对被刀刃命中的所有敌人造成90/135/180(+0.8)物理伤害，获得40/55/70%移动速度提升，并且变为隐形状态，最多持续2.5秒。当隐形状态结束时，刀刃会汇聚向泰隆处，对被它们穿过的敌人再次造成相同伤害。', 'public/LOL_IMG/tailong/jn5.png', '如果泰隆用一次攻击或【Q诺克萨斯式外交】取消掉隐形状态，那么刀刃则会汇聚向他的目标处。');
INSERT INTO `h_jineng` VALUES ('9', '3', '致盲吹箭', '对目标造成80/125/170/215/260(+0.8)魔法伤害并使目标致盲1.5/1.75/2/2.25/2.5秒。', 'public/LOL_IMG/timo/jn1.png', '伤害：80/125/170/215/260\r\n持续时间： 1.5/1.75/2/2.25/2.5\r\n消耗法力： 70/75/80/85/90');
INSERT INTO `h_jineng` VALUES ('10', '3', '小莫快跑', '被动：提莫的移动速度提升10/14/18/22/26%。若在最近5秒内受到来自敌方英雄或防御塔的伤害，则此效果移除。', 'public/LOL_IMG/timo/jn2.png', '主动：提莫急速奔跑，获得此技能被动部分的双倍加成，持续3秒。这个加速效果在被攻击时也不会移除。\r\n基础移动速度加成：10/14/18/22/26%');
INSERT INTO `h_jineng` VALUES ('11', '3', '毒性射击', '提莫的普通攻击会使目标中毒，立即造成10/20/30/40/50(+0.3)魔法伤害，并在随后的4秒里每秒造成6/12/18/24/30(+0.1)魔法伤害。\r\n', 'public/LOL_IMG/timo/jn3.png', '立即伤害： 10/20/30/40/50\r\n每秒伤害 ： 6/12/18/24/30');
INSERT INTO `h_jineng` VALUES ('12', '3', '种蘑菇', '使用一个储存的蘑菇来投掷一个陷阱，如果被敌人踩到，陷阱会爆炸，将毒散发到附近的敌人身上，减缓他们30/40/50%的移动速度，并在4秒里持续造成共200/325/450(+0.5)魔法伤害。', 'public/LOL_IMG/timo/jn4.png', '陷阱持续5分钟，并且需要花费1秒来部署和潜行。如果一个被投掷的陷阱命中了另一个陷阱，那么它会向前弹跳到3/4/5个提莫身位的距离，然后才开始布置。');
INSERT INTO `h_jineng` VALUES ('13', '4', '流星坠落', '召唤一颗流星，从索拉卡所处的位置落向目标地点。站在爆炸范围内的敌人会受到70/110/150/190/230(+0.35)魔法伤害并被减速30%，持续2秒。', 'public/LOL_IMG/suolaka/jn1.png', '如果此技能命中了一个敌方英雄，那么索拉卡会获得持续4秒的活力焕发效果。这个效果每秒为索拉卡回复14/16/18/20/22(+)生命值，并且在索拉卡不朝着敌方英雄移动时提供10%移动速度。');
INSERT INTO `h_jineng` VALUES ('14', '4', '星之灌注', '为目标友军回复80/110/140/170/200(+0.6)生命值', 'public/LOL_IMG/suolaka/jn2.png', '如果在身上带有活力焕发效果时施放，那么索拉卡会将此效果的收益提供给目标英雄，持续秒。');
INSERT INTO `h_jineng` VALUES ('15', '4', '星体结界', '在目标区域创造一个持续1.5秒的结界，对施法半径内的敌方英雄造成70/110/150/190/230(+0.4)魔法伤害。敌方英雄在结界内将一直被沉默，直到走出结界为止。', 'public/LOL_IMG/suolaka/jn3.png', '当结界消失时，所有仍在结界内的敌方英雄将被禁锢1/1.25/1.5/1.75/2秒，并受到70/110/150/190/230(+0.4)魔法伤害。');
INSERT INTO `h_jineng` VALUES ('16', '4', '祈愿', '召唤神圣的能量来为每个友方英雄回复150/250/350(+0.55)生命值。祈愿会对每个生命值低于40%的友方英雄造成150%的回复效果。', 'public/LOL_IMG/suolaka/jn4.png', '生命回复：150/250/350\r\n冷却时间：160/145/130');
INSERT INTO `h_jineng` VALUES ('17', '5', '火焰烙印', '布兰德向前方放出一团火球，可造成80/110/140/170/200(+0.55)魔法伤害。', 'public/LOL_IMG/huonan/jn1.png', '炽热之焰: 如果目标带有【烈焰焚身】效果，【火焰烙印】将使目标晕眩1.5秒。');
INSERT INTO `h_jineng` VALUES ('18', '5', '烈焰之柱', '在短暂的延迟后，布兰德会在目标区域创造一根烈焰之柱。对范围内的敌人造成75/120/165/210/255(+0.6)魔法伤害', 'public/LOL_IMG/huonan/jn2.png', '炽热之焰:【W烈焰之柱】会对带有【烈焰焚身】效果的单位额外造成25%伤害。');
INSERT INTO `h_jineng` VALUES ('19', '5', '烈火燃烧', '布兰德在目标身上引发一阵强力的爆裂，造成70/90/110/130/150(+0.35)魔法伤害。', 'public/LOL_IMG/huonan/jn3.png', '炽热之焰:如果目标带有【烈焰焚身】效果，那么此技能会扩散至目标附近的敌人');
INSERT INTO `h_jineng` VALUES ('20', '5', '烈焰风暴', '布兰德释放一颗破坏力极强的火焰之种，每次弹跳造成100/200/300(+0.25)魔法伤害，最多可弹跳5次。弹跳会优先以带有即将满层的【炽热之焰】效果的英雄为目标。', 'public/LOL_IMG/huonan/jn4.png', '炽热之焰: 如果目标带有【烈焰焚身】效果，烈焰风暴将暂时使目标减速30/45/60%。');

-- ----------------------------
-- Table structure for h_kezhi
-- ----------------------------
DROP TABLE IF EXISTS `h_kezhi`;
CREATE TABLE `h_kezhi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Gay` int(11) DEFAULT NULL COMMENT '英雄关系：1为搭档；2为压制关系；3为被压制关系',
  `H_id` int(11) DEFAULT NULL COMMENT '英雄id',
  `Gay_one` int(11) DEFAULT NULL,
  `Gay_two` int(11) DEFAULT NULL,
  `Gay_tips` varchar(255) DEFAULT NULL COMMENT '贴士',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_kezhi
-- ----------------------------
INSERT INTO `h_kezhi` VALUES ('1', '3', '1', '1', '2', '作为射手很容易被刺客秒杀');
INSERT INTO `h_kezhi` VALUES ('2', '2', '2', '2', '1', '作为刺客很容易秒杀对手');

-- ----------------------------
-- Table structure for h_liebiao
-- ----------------------------
DROP TABLE IF EXISTS `h_liebiao`;
CREATE TABLE `h_liebiao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Hero_name` varchar(255) DEFAULT NULL COMMENT '英雄名称',
  `Hero_title` varchar(255) DEFAULT NULL,
  `Hero_position` varchar(255) DEFAULT NULL COMMENT '英雄定位',
  `Hero_live` varchar(255) DEFAULT NULL COMMENT '生存能力',
  `Hero_ hurt` varchar(255) DEFAULT NULL COMMENT '攻击伤害',
  `Hero_ Skill` varchar(255) DEFAULT NULL COMMENT '技能效果',
  `Hero_difficulty` varchar(255) DEFAULT NULL COMMENT '上手难度',
  `default_face` varchar(255) DEFAULT NULL,
  `default_mode` varchar(255) DEFAULT NULL COMMENT '默认皮肤路径',
  `Hero_story` text COMMENT '英雄故事',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_liebiao
-- ----------------------------
INSERT INTO `h_liebiao` VALUES ('1', '格雷福斯', '法外狂徒', '射手', '7', '6', '4', '6', 'public/LOL_IMG/nanqiang/touxiang.png', 'public/LOL_IMG/nanqiang/pf1.jpg', '格雷福斯小时候生活在比尔吉沃特的码头货仓小巷里，他很快就学会了打斗、偷窃，还有许多受用多年的“手艺”。后来，年少的他躲在了一艘远洋货船舱底的污水中，偷渡到了大陆，从此开始了盗窃、欺骗和赌博的生活。他居无定所，四处漂泊。直到有一天，格雷福斯遇到了改变自己一生的人，他就是现在被人称为崔斯特的赌棍。两个人都在对方身上看到了与自己相同的、对于冒险的挑战和热爱，于是二人开始了他们几乎长达十年之久的畸形的合作关系。');
INSERT INTO `h_liebiao` VALUES ('2', '泰隆', '刀锋之影', '刺客', '3', '6', '3', '5', 'public/LOL_IMG/tailong/touxiang.png', 'public/LOL_IMG/tailong/pf1.jpg', '“瓦洛兰的三个最能取人性命的刃武大师，都与杜 · 克卡奥家族有关系：我的父亲，我，还有泰隆。来挑战我们吧，如果你够胆的话。”\r\n——卡特琳娜 · 杜 · 克卡奥');
INSERT INTO `h_liebiao` VALUES ('3', '提莫', '迅捷斥候', '射手', '5', '5', '5', '5', 'public/LOL_IMG/timo/touxiang.png', 'public/LOL_IMG/timo/pf1.jpg', '在班德尔城，提莫是流传于兄弟姐妹中的一则传奇。就约德尔人而言，提莫有点与众不同。虽然他喜欢与其他约德尔人结伴，但他也常常坚持要单枪匹马地执行保卫班德尔城的任务。他为人真诚，性情温和，然而一旦进入战斗，提莫就像变了个人似的，在巡逻时会果断了结那些敌人的性命。在他还是一名年轻的新兵之时，军事训练的教官与其他学员就发现——虽然提莫平常和蔼可亲，很有魅力，然而一旦搏斗练习开始，他就变得异常严肃，效率极高。提莫的上司很快便引导他朝着“主舰斥候队”的方向发展。“主舰斥候队”是班德尔城最富盛名的特种部队之一，与麦林突击队齐名。');
INSERT INTO `h_liebiao` VALUES ('5', '布兰德', '复仇焰魂', '法师', '6', '5', '8', '3', 'public/LOL_IMG/huonan/touxiang.png', 'public/LOL_IMG/huonan/pf1.jpg', '在遥远的洛克法，有个叫基根·诺和的海上掠夺者。和洛克法的其他人一样，基根和他的船员到处航行，掠夺那些倒霉蛋的财物。对某些人而言，他掠夺财物，是个魔头。但对另外一些人而言，他和大众一样只是个平凡的强盗。某个夜晚，他们正驶过北冰洋时，发现冰冻废土上闪动着奇光。这些奇光似乎有种催眠效果，将他们吸引到身边，船员们如同飞蛾扑火般涌上来。经过艰难地跋涉后，他们来到了被古代符文覆盖着的窑洞。由于符文非常古老，他们无从解读。在基根的带领下，他们走进窑洞。在那里，他们发现一个完美的冰笼，冰笼里有一团跳动着的火焰。这种东西按理说根本就不可能燃烧，尤其是在这种地方。然而，火焰的跳动就像塞壬海妖的歌唱那样具有催眠的魔力，令人着迷，充满诱惑。当其他人都望而却步时，基根却无法抑制自己的好奇心，逐渐靠近，并伸出了手……');
INSERT INTO `h_liebiao` VALUES ('4', '索拉卡', '众星之子', '辅助', '7', '7', '7', '7', 'public/LOL_IMG/suokala/touxiang.png', 'public/LOL_IMG/suolaka/pf1.jpg', '索拉卡，艾欧尼亚的众星之子，是同族的第一人。当人们蜂涌着去开发瓦洛兰丰富的魔法能量时，她却首开先河来探索宇宙自身的魔法能量——宇宙仿佛一个自成体系的天体汪洋，符文之地就遨游于其他相似天体之中。虽然符文之地的魔法师大多局限于符文的有限力量（尽管这些力量异常巨大）中，但总有一些人会另辟蹊径，探索魔法的更深处。索拉卡的探索超越了符文之地的天空，令她能召唤宇宙星辰的力量，而她的进化也远在她的族人之上。这股力量令她发生了不可思议的改变——她也因此以“众星之子”闻名于世。');

-- ----------------------------
-- Table structure for h_pifu
-- ----------------------------
DROP TABLE IF EXISTS `h_pifu`;
CREATE TABLE `h_pifu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `H_id` int(11) DEFAULT NULL COMMENT '英雄id',
  `src` varchar(255) DEFAULT NULL COMMENT '皮肤路径',
  `Face_src` varchar(255) DEFAULT NULL,
  `face_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of h_pifu
-- ----------------------------
INSERT INTO `h_pifu` VALUES ('1', '1', 'public/LOL_IMG/nanqiang/pf2.png', 'public/LOL_IMG/nanqiang/touxiang.png', '怒之火炮');
INSERT INTO `h_pifu` VALUES ('2', '1', 'public/LOL_IMG/nanqiang/pf3.png', 'public/LOL_IMG/nanqiang/touxiang.png', '杀出重围');
INSERT INTO `h_pifu` VALUES ('3', '1', 'public/LOL_IMG/nanqiang/pf4.png', 'public/LOL_IMG/nanqiang/touxiang.png', '黑帮教父');
INSERT INTO `h_pifu` VALUES ('4', '1', 'public/LOL_IMG/nanqiang/pf5.png', 'public/LOL_IMG/nanqiang/touxiang.png', '防爆士兵');
INSERT INTO `h_pifu` VALUES ('5', '1', 'public/LOL_IMG/nanqiang/pf6.png', 'public/LOL_IMG/nanqiang/touxiang.png', '泳池派对');
INSERT INTO `h_pifu` VALUES ('6', '1', 'public/LOL_IMG/nanqiang/pf7.png', 'public/LOL_IMG/nanqiang/touxiang.png', '无情重炮');
INSERT INTO `h_pifu` VALUES ('7', '1', 'public/LOL_IMG/nanqiang/pf8.png', 'public/LOL_IMG/nanqiang/touxiang.png', '冰雪节');
INSERT INTO `h_pifu` VALUES ('8', '1', 'public/LOL_IMG/nanqiang/pf9.png', 'public/LOL_IMG/nanqiang/touxiang.png', '胜利枪神');
INSERT INTO `h_pifu` VALUES ('9', '2', 'public/LOL_IMG/tailong/pf2.png', 'public/LOL_IMG/tailong/touxiang.png', '刺客信条');
INSERT INTO `h_pifu` VALUES ('10', '2', 'public/LOL_IMG/tailong/pf3.png', 'public/LOL_IMG/nanqiang/touxiang.png', '血色精锐');
INSERT INTO `h_pifu` VALUES ('11', '2', 'public/LOL_IMG/tailong/pf4.png', 'public/LOL_IMG/nanqiang/touxiang.png', '银龙裁决');
INSERT INTO `h_pifu` VALUES ('12', '2', 'public/LOL_IMG/tailong/pf5.png', 'public/LOL_IMG/nanqiang/touxiang.png', 'SSW');
INSERT INTO `h_pifu` VALUES ('13', '2', 'public/LOL_IMG/tailong/pf6.png', 'public/LOL_IMG/nanqiang/touxiang.png', '腥红之月');
INSERT INTO `h_pifu` VALUES ('14', '3', 'public/LOL_IMG/timo/pf2.png', 'public/LOL_IMG/timo/touxiang.png', '圣诞开心果');
INSERT INTO `h_pifu` VALUES ('15', '3', 'public/LOL_IMG/timo/pf3.png', 'public/LOL_IMG/timo/touxiang.png', '军情五处');
INSERT INTO `h_pifu` VALUES ('16', '3', 'public/LOL_IMG/timo/pf4.png', 'public/LOL_IMG/timo/touxiang.png', '密林猎手');
INSERT INTO `h_pifu` VALUES ('17', '3', 'public/LOL_IMG/timo/pf5.png', 'public/LOL_IMG/timo/touxiang.png', '提莫:\'约德尔人的一大步\'');
INSERT INTO `h_pifu` VALUES ('18', '3', 'public/LOL_IMG/timo/pf6.png', 'public/LOL_IMG/timo/touxiang.png', '兔宝宝');
INSERT INTO `h_pifu` VALUES ('19', '3', 'public/LOL_IMG/timo/pf7.png', 'public/LOL_IMG/timo/touxiang.png', '约德尔国队长');
INSERT INTO `h_pifu` VALUES ('20', '3', 'public/LOL_IMG/timo/pf8.png', 'public/LOL_IMG/timo/touxiang.png', '熊猫');
INSERT INTO `h_pifu` VALUES ('21', '3', 'public/LOL_IMG/timo/pf9.png', 'public/LOL_IMG/timo/touxiang.png', '欧米伽小队');
INSERT INTO `h_pifu` VALUES ('22', '3', 'public/LOL_IMG/timo/pf10.png', 'public/LOL_IMG/timo/touxiang.png', '大魔王');
INSERT INTO `h_pifu` VALUES ('23', '4', 'public/LOL_IMG/suokala/pf2.png', 'public/LOL_IMG/suolaka/touxiang.png', '森林女神');
INSERT INTO `h_pifu` VALUES ('24', '4', 'public/LOL_IMG/suokala/pf3.png', 'public/LOL_IMG/suolaka/touxiang.png', '月光女神阿忒弥斯');
INSERT INTO `h_pifu` VALUES ('25', '4', 'public/LOL_IMG/suokala/pf4.png', 'public/LOL_IMG/suolaka/touxiang.png', '圣洁化身');
INSERT INTO `h_pifu` VALUES ('26', '4', 'public/LOL_IMG/suokala/pf5.png', 'public/LOL_IMG/suolaka/touxiang.png', '灵魂收割者');
INSERT INTO `h_pifu` VALUES ('27', '4', 'public/LOL_IMG/suokala/pf6.png', 'public/LOL_IMG/suolaka/touxiang.png', '蕉泥座人');
INSERT INTO `h_pifu` VALUES ('28', '4', 'public/LOL_IMG/suokala/pf7.png', 'public/LOL_IMG/suolaka/touxiang.png', '源代码');
INSERT INTO `h_pifu` VALUES ('29', '4', 'public/LOL_IMG/suokala/pf8.png', 'public/LOL_IMG/suolaka/touxiang.png', '星之守护者');
INSERT INTO `h_pifu` VALUES ('30', '5', 'public/LOL_IMG/huonan/pf2.png', 'public/LOL_IMG/huonan/touxiang.png', '末日余生');
INSERT INTO `h_pifu` VALUES ('31', '5', 'public/LOL_IMG/huonan/pf3.png', 'public/LOL_IMG/huonan/touxiang.png', 'V字仇杀者');
INSERT INTO `h_pifu` VALUES ('32', '5', 'public/LOL_IMG/huonan/pf4.png', 'public/LOL_IMG/huonan/touxiang.png', '冰晶之核');
INSERT INTO `h_pifu` VALUES ('33', '5', 'public/LOL_IMG/huonan/pf5.png', 'public/LOL_IMG/huonan/touxiang.png', '丧尸');
INSERT INTO `h_pifu` VALUES ('34', '5', 'public/LOL_IMG/huonan/pf6.png', 'public/LOL_IMG/huonan/touxiang.png', '灵魂烈焰');
INSERT INTO `h_pifu` VALUES ('35', '5', 'public/LOL_IMG/huonan/pf7.png', 'public/LOL_IMG/huonan/touxiang.png', '战场BOSS');

-- ----------------------------
-- Table structure for jy_jineng
-- ----------------------------
DROP TABLE IF EXISTS `jy_jineng`;
CREATE TABLE `jy_jineng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `H_id` int(11) DEFAULT NULL,
  `first` int(11) DEFAULT NULL,
  `second` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jy_jineng
-- ----------------------------
INSERT INTO `jy_jineng` VALUES ('1', '1', '3', '2');
INSERT INTO `jy_jineng` VALUES ('2', '2', '6', '5');
INSERT INTO `jy_jineng` VALUES ('3', '3', '11', '9');
INSERT INTO `jy_jineng` VALUES ('4', '4', '14', '13');
INSERT INTO `jy_jineng` VALUES ('5', null, null, null);

-- ----------------------------
-- Table structure for jy_tianfu
-- ----------------------------
DROP TABLE IF EXISTS `jy_tianfu`;
CREATE TABLE `jy_tianfu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `H_id` int(11) DEFAULT NULL COMMENT '英雄id\r\n',
  `One` int(11) DEFAULT NULL,
  `Two` int(11) DEFAULT NULL,
  `Three` int(11) DEFAULT NULL,
  `I_Tips` varchar(255) DEFAULT NULL COMMENT '贴士',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jy_tianfu
-- ----------------------------
INSERT INTO `jy_tianfu` VALUES ('1', '1', '1', '2', '3', '首选精密 次选主宰');
INSERT INTO `jy_tianfu` VALUES ('2', '2', '2', '3', '1', '首选主宰 次选巫术');
INSERT INTO `jy_tianfu` VALUES ('3', '3', '3', '1', '2', '首选巫术 次选主宰');
INSERT INTO `jy_tianfu` VALUES ('4', '4', '5', '4', '3', '首选坚决 次选启迪');
INSERT INTO `jy_tianfu` VALUES ('5', '5', '3', '1', '4', '首选巫术 次选精密');

-- ----------------------------
-- Table structure for jy_zhs_jineng
-- ----------------------------
DROP TABLE IF EXISTS `jy_zhs_jineng`;
CREATE TABLE `jy_zhs_jineng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `H_id` int(11) DEFAULT NULL COMMENT '英雄id',
  `R_first` int(11) DEFAULT NULL COMMENT '推荐一',
  `R_Second` int(11) DEFAULT NULL COMMENT '推荐二',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jy_zhs_jineng
-- ----------------------------
INSERT INTO `jy_zhs_jineng` VALUES ('1', '1', '1', '2');
INSERT INTO `jy_zhs_jineng` VALUES ('2', '2', '2', '1');
INSERT INTO `jy_zhs_jineng` VALUES ('3', '3', '1', '6');
INSERT INTO `jy_zhs_jineng` VALUES ('4', '4', '1', '7');
INSERT INTO `jy_zhs_jineng` VALUES ('5', '5', '1', '2');

-- ----------------------------
-- Table structure for jy_zhuangbei
-- ----------------------------
DROP TABLE IF EXISTS `jy_zhuangbei`;
CREATE TABLE `jy_zhuangbei` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `H_id` int(11) DEFAULT NULL COMMENT '英雄id',
  `E_One` int(11) DEFAULT NULL,
  `E_Two` int(11) DEFAULT NULL,
  `E_Three` int(11) DEFAULT NULL,
  `E_Four` int(11) DEFAULT NULL,
  `E_Five` int(11) DEFAULT NULL,
  `E_Six` int(11) DEFAULT NULL,
  `E_tips` varchar(255) DEFAULT NULL COMMENT '贴士',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jy_zhuangbei
-- ----------------------------
INSERT INTO `jy_zhuangbei` VALUES ('1', '1', '2', '3', '4', '5', '6', '7', '猛的一批');
INSERT INTO `jy_zhuangbei` VALUES ('4', '4', '23', '2', '24', '25', '26', '11', '奶飞天');
INSERT INTO `jy_zhuangbei` VALUES ('5', '5', '2', '27', '28', '29', '30', '20', '团战强无敌');
INSERT INTO `jy_zhuangbei` VALUES ('2', '2', '10', '11', '12', '13', '14', '15', '伤害爆炸');
INSERT INTO `jy_zhuangbei` VALUES ('3', '3', '22', '2', '5', '19', '20', '21', '吊的不行');

-- ----------------------------
-- Table structure for tianfu
-- ----------------------------
DROP TABLE IF EXISTS `tianfu`;
CREATE TABLE `tianfu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `I_img` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `I_Name` varchar(255) DEFAULT NULL COMMENT '铭文名称',
  `I_content` varchar(255) DEFAULT NULL COMMENT '铭文属性',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tianfu
-- ----------------------------
INSERT INTO `tianfu` VALUES ('1', 'public/LOL_IMG/tianfu/JM.png', '精密', '强化攻击和持续伤害');
INSERT INTO `tianfu` VALUES ('5', 'public/LOL_IMG/tianfu/jj.png', '坚决', '耐久和控制');
INSERT INTO `tianfu` VALUES ('2', 'public/LOL_IMG/tianfu/ZZ.png', '主宰', '18%攻击速度');
INSERT INTO `tianfu` VALUES ('4', 'public/LOL_IMG/tianfu/WS.png', '启迪', '+130生命值');
INSERT INTO `tianfu` VALUES ('3', 'public/LOL_IMG/tianfu/WS.png', '巫术', '强化技能和资源控制');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL DEFAULT '',
  `user_nickname` varchar(30) NOT NULL DEFAULT '',
  `user_pwd` varchar(32) NOT NULL,
  `user_addtime` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1111111111111111', '1111111111111111', 'fa1d3eb08a879de9a4cd9995a1aa91e1', '2017-08-18 09:52:53');
INSERT INTO `user` VALUES ('2', '1111111111111111', '1111111111111111', 'fa1d3eb08a879de9a4cd9995a1aa91e1', '2017-08-18 10:09:54');
INSERT INTO `user` VALUES ('3', 'admin123', 'admin123', '0192023a7bbd73250516f069df18b500', '2017-08-18 10:38:17');
INSERT INTO `user` VALUES ('4', 'admin123', 'admin123', '0192023a7bbd73250516f069df18b500', '2017-08-18 10:38:25');
INSERT INTO `user` VALUES ('5', 'admin1', 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', '2017-08-18 10:51:34');

-- ----------------------------
-- Table structure for zhs_jineng
-- ----------------------------
DROP TABLE IF EXISTS `zhs_jineng`;
CREATE TABLE `zhs_jineng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `S_img` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `S_Name` varchar(255) DEFAULT NULL COMMENT '技能名称',
  `LV` varchar(255) DEFAULT NULL COMMENT '解锁等级',
  `cont` varchar(255) DEFAULT NULL COMMENT '文字介绍',
  `con_img` varchar(255) DEFAULT NULL COMMENT '效果图',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zhs_jineng
-- ----------------------------
INSERT INTO `zhs_jineng` VALUES ('1', 'public/LOL_IMG/zhs_jineng/sx.png', '闪现', 'LV.1解锁', '120秒CD：使英雄朝着你的指针所停的区域瞬间传送一小段距离', 'public/LOL_IMG/zhs_jineng/sx_img.png');
INSERT INTO `zhs_jineng` VALUES ('2', 'public/LOL_IMG/zhs_jineng/yr.png', '引燃', 'LV.1解锁', '90秒CD：引燃是对单体敌方目标施放的持续性伤害技能，在5秒的持续时间里造成70-410（取决于英雄等级）真实伤害，获得目标的视野，并减少目标所受的治疗和回复效果。', 'public/LOL_IMG/zhs_jineng/yr_img.png');
INSERT INTO `zhs_jineng` VALUES ('3', 'public/LOL_IMG/zhs_jineng/cj.png', '惩戒', 'LV.1解锁', '对目标史诗野怪、大型野怪或敌方小兵造成390-1000（取决于英雄等级）点真实伤害。', 'public/LOL_IMG/zhs_jineng/cj_img.png');
INSERT INTO `zhs_jineng` VALUES ('4', 'public/LOL_IMG/zhs_jineng/pz.png', '屏障', 'LV.1解锁', '为你的英雄套上护盾，吸收115-455（取决于英雄等级）点伤害，持续2秒。', 'public/LOL_IMG/zhs_jineng/pz_img.png');
INSERT INTO `zhs_jineng` VALUES ('5', 'public/LOL_IMG/zhs_jineng/qxs.png', '清晰术', 'LV.1解锁', '为你的英雄和周围的友军回复40%的最大法力值。', 'public/LOL_IMG/zhs_jineng/qxs_img.png');
INSERT INTO `zhs_jineng` VALUES ('6', 'public/LOL_IMG/zhs_jineng/yljb.png', '幽灵疾步', 'LV.1解锁', '你的英雄在移动时会无视单位的碰撞体积，移动速度增加27%，持续10秒。', 'public/LOL_IMG/zhs_jineng/yljb_img.png');
INSERT INTO `zhs_jineng` VALUES ('7', 'public/LOL_IMG/zhs_jineng/zl.png', '治疗术', 'LV.1解锁', '为你和目标友军英雄回复95-345（取决于英雄等级）生命值，并为你和目标友军英雄提供30%移动速度加成，持续1秒。若目标近期已受到过其它治疗术的影响，则治疗术对目标产生的治疗效果减半。', 'public/LOL_IMG/zhs_jineng/zl_img.png');

-- ----------------------------
-- Table structure for zhuangbei
-- ----------------------------
DROP TABLE IF EXISTS `zhuangbei`;
CREATE TABLE `zhuangbei` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `E_name` varchar(255) DEFAULT NULL COMMENT '装备名称',
  `E_img` varchar(255) DEFAULT NULL COMMENT '装备图片路径\r\n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zhuangbei
-- ----------------------------
INSERT INTO `zhuangbei` VALUES ('1', '多兰剑', 'public/LOL_IMG/zhuangbei/zb1.png');
INSERT INTO `zhuangbei` VALUES ('2', '血瓶', 'public/LOL_IMG/zhuangbei/zb2.png');
INSERT INTO `zhuangbei` VALUES ('3', '死亡之舞', 'public/LOL_IMG/zhuangbei/zb3.png');
INSERT INTO `zhuangbei` VALUES ('4', '狂战士胫甲', 'public/LOL_IMG/zhuangbei/zb4.png');
INSERT INTO `zhuangbei` VALUES ('5', '幻影之舞', 'public/LOL_IMG/zhuangbei/zb5.png');
INSERT INTO `zhuangbei` VALUES ('6', '无尽之刃', 'public/LOL_IMG/zhuangbei/zb6.png');
INSERT INTO `zhuangbei` VALUES ('7', '疾射火炮', 'public/LOL_IMG/zhuangbei/zb7.png');
INSERT INTO `zhuangbei` VALUES ('8', '多米尼克领主的致意', 'public/LOL_IMG/zhuangbei/zb8.png');
INSERT INTO `zhuangbei` VALUES ('9', '复用型药水', 'public/LOL_IMG/zhuangbei/zb9.png');
INSERT INTO `zhuangbei` VALUES ('22', '多兰戒', 'public/LOL_IMG/zhuangbei/zb22.png');
INSERT INTO `zhuangbei` VALUES ('21', '纳什之牙', 'public/LOL_IMG/zhuangbei/zb21.png');
INSERT INTO `zhuangbei` VALUES ('20', '法师之靴', 'public/LOL_IMG/zhuangbei/zb20.png');
INSERT INTO `zhuangbei` VALUES ('19', '兰德里的折磨', 'public/LOL_IMG/zhuangbei/zb19.png');
INSERT INTO `zhuangbei` VALUES ('18', '守护天使', 'public/LOL_IMG/zhuangbei/zb18.png');
INSERT INTO `zhuangbei` VALUES ('11', '明朗之靴', 'public/LOL_IMG/zhuangbei/zb11.png');
INSERT INTO `zhuangbei` VALUES ('12', '幽梦之灵', 'public/LOL_IMG/zhuangbei/zb12.png');
INSERT INTO `zhuangbei` VALUES ('13', '黑色切割者', 'public/LOL_IMG/zhuangbei/zb13.png');
INSERT INTO `zhuangbei` VALUES ('14', '德拉克萨的暮刃', 'public/LOL_IMG/zhuangbei/zb14.png');
INSERT INTO `zhuangbei` VALUES ('15', '贪欲九头蛇', 'public/LOL_IMG/zhuangbei/zb15.png');
INSERT INTO `zhuangbei` VALUES ('10', '长剑', 'public/LOL_IMG/zhuangbei/zb10.png');
INSERT INTO `zhuangbei` VALUES ('17', '夜之锋刃', 'public/LOL_IMG/zhuangbei/zb17.png');
INSERT INTO `zhuangbei` VALUES ('16', '玛莫提乌斯之噬', 'public/LOL_IMG/zhuangbei/zb16.png');
INSERT INTO `zhuangbei` VALUES ('23', '上古钱币', 'public/LOL_IMG/zhuangbei/zb23.png');
INSERT INTO `zhuangbei` VALUES ('24', '监视图腾', 'public/LOL_IMG/zhuangbei/zb24.png');
INSERT INTO `zhuangbei` VALUES ('25', '飞升护符', 'public/LOL_IMG/zhuangbei/zb25.png');
INSERT INTO `zhuangbei` VALUES ('26', '救赎', 'public/LOL_IMG/zhuangbei/zb26.png');
INSERT INTO `zhuangbei` VALUES ('27', '灭世者的死亡之帽', 'public/LOL_IMG/zhuangbei/zb27.png');
INSERT INTO `zhuangbei` VALUES ('28', '瑞莱的冰晶节杖', 'public/LOL_IMG/zhuangbei/zb28.png');
INSERT INTO `zhuangbei` VALUES ('29', '中娅沙漏', 'public/LOL_IMG/zhuangbei/zb29.png');
INSERT INTO `zhuangbei` VALUES ('30', '女妖面纱', 'public/LOL_IMG/zhuangbei/zb30.png');
