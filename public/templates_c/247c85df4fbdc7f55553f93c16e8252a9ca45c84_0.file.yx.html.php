<?php
/* Smarty version 3.1.30, created on 2018-01-03 02:26:09
  from "D:\wamp64\www\ASiDiFen\Application\Font\View\yx.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a4c3f418eaa15_90044128',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '247c85df4fbdc7f55553f93c16e8252a9ca45c84' => 
    array (
      0 => 'D:\\wamp64\\www\\ASiDiFen\\Application\\Font\\View\\yx.html',
      1 => 1514946362,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a4c3f418eaa15_90044128 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="public/css/iconfont.css">
	<link rel="stylesheet" href="public/BootStrap/css/bootstrap.css">
    <link rel="stylesheet" href="public/layui/css/layui.css">
    <?php echo '<script'; ?>
 src="public/jquery-3.0.0/jquery-3.0.0.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="public/layui/layui.js"><?php echo '</script'; ?>
>
	<style type="text/css">
		/* Header头部CSS */
		/* 响应式显示menu菜单按钮，自定义三道杠 */
		.icon-bar {
			display: block;
			width: 25px;
			height: 3px;
			border-radius: 3px;
			background-color: grey;
		}
		.icon-bar + .icon-bar {
		  	margin-top: 5px;
		}

		ul{
			padding: 0px;
			margin: 0px;
			list-style-position: inside;
		}
		.header{
			position: absolute;
			right: 0;
			left: 0;
			top: 0;
			z-index: 1030;
		}
		.header_flex{
			height: 90px;
		}
		.container-mcd{
			display: flex;
			flex-direction: row;
			background-color: #000;
			position: relative;
			width: 100%;
		}
		.container-mcd .item{
			flex:1 1 auto;
			height: 70px;
			/* border: 1px solid red; */
			color: grey;
			text-align: center;
			/* transition: all 0.2s; */
			position: relative;
            margin-left: 20px;
		}
		.item_content{
			width: 100%;
			height: 100%;
			box-sizing: border-box;
			position: absolute;
			top: 0px;
			left: 0px;
			margin-top: 20px;
		}
		.container-mcd .logo{
			flex: none;
			width: 224px;
			height: 90px;
			margin: 0 auto;
			background-image: url('public/img/logo.jpg');
			background-repeat: no-repeat;
			background-position: center;
			background-size: 100%;

			/* transition: all 0.2s; */
		}
		.container-mcd .item .item_content p{
            overflow:hidden;
            text-overflow:ellipsis;/*文字溢出的部分隐藏并用省略号代替*/
            white-space:nowrap;/*文本不自动换行*/
		}

		/* 遮罩背景 */
		.mask{

		}
		/* 鼠标浮动框 */
		.menu_list{
			display: none;
			/* height: 100px; */
			padding-top: 50px;
			padding-bottom: 50px;
			/* margin-top: 120px; */
			background-color: rgba(0,0,0,0.5);
			background-image: linear-gradient(180deg,rgba(0,0,0,0.5) 0%,rgba(0,0,0,0.2) 100%);

			z-index: 1030;
		}
		.mcd_food{
			width: 100%;
			height: 100%;
			display: flex;

			display: none;
		}

		.item_content span{
			color: whitesmoke;
			font-size: 20px;
			display: block;
            overflow:hidden;
            text-overflow:ellipsis;/*文字溢出的部分隐藏并用省略号代替*/
            white-space:nowrap;/*文本不自动换行*/

		}
         .tou{
			 width: 83px;
			 margin-top: 7px;
			 margin-left: 8px;
		 }
         .hean_left{
             overflow:hidden;
             text-overflow:ellipsis;/*文字溢出的部分隐藏并用省略号代替*/
             white-space:nowrap;/*文本不自动换行*/
         }
         .food_classic{
             margin-top: -30px;
         }
         .food_classic:after{
             content: '';
             display: inline-block;
             clear: both;
         }
         .food_classic ul{
            list-style-type: none;
             float: left;
             margin-left: 90px;
             text-align: center;
         }
        .food_classic ul li{
            margin-top: 10px;
        }
        .food_classic ul li a{
            text-decoration: none;
            color: whitesmoke;
        }
        .food_classic .first{
            margin-left: 120px;
        }
        .headimage img{
            width: 100%;
            /* z-index: 999; */
            margin-top: 90px;
        }

        .font{
            position: relative;
            width: 259px;
            height:55px;
            background-image: url('public/img/font.png');
            background-repeat: no-repeat;
            background-size: 100%;
            left: 830px;
            top: 578px;
            z-index: 999;
        }
        .fadajing{
            height: 32px;
            margin-top: 28px;
            width: 430px;
            margin-left: 30px;
        }
        .sousuo{
        cursor: pointer;
        }
        .Navbar{
            height: 60px;
            width: 100%;
            box-shadow:2px 0px 20px gray;
        }
        .Navbar ul{
            list-style-type: none;
            text-decoration: none;
            margin-top: -1px;
            height: 60px;
        }

        .Navbar ul li{
            float: left;
            font-size: 24px;
            font-weight: 500;
            cursor: pointer;
            margin-left: 90px;
            line-height: 60px;
            overflow: hidden;
            text-overflow: ;
            white-space: nowrap;
        }
        .Navbar ul:after{
            content: '';
            display: block;
            clear: both;
        }
        .yx{
             width: 66px; 
             height: 66px; 
             margin-left: 20px;
             margin-top: 20px;
        }
	</style>
    <title>游戏资料-英雄联盟官方网站-腾讯游戏</title>
    <link rel="icon" type="image/png" href="111.png">
</head>
<body>
	<!-- 头部带导航 -->
	<div class="header" >
        <div style="width: 100%;height: 90px;background: black">
            <div class="header_flex col-lg-10 col-lg-offset-1">
                <div class="container-mcd ">
                    <div class="logo"></div>
                    <div class="item hidden-xs" data-index="1">

                        <div class="item_content">
                            <span class="">游戏资料</span>
                            <p>GAME INFO</p>
                        </div>

                    </div>
                    <div class="item hidden-xs" data-index="1">
                        <div class="item_content">
                            <span class="">商城/合作</span>
                            <p>STORE</p>
                        </div>
                    </div>
                    <div class="item hidden-xs" data-index="1">
                        <div class="item_content">
                            <span class="">用户互动</span>
                            <p>COMMUNITY</p>
                        </div>
                    </div>
                    <div class="item hidden-xs" data-index="1">
                        <div class="item_content">
                            <span class="">赛事中心</span>
                            <p>EVENTS</p>
                        </div>
                    </div>
                    <div class="item hidden-xs" data-index="1">
                        <div class="item_content">
                            <span class="">自助系统</span>
                            <p>SYSTEM</p>
                        </div>
                    </div>
                    <input type="text" class="fadajing" placeholder="请输入搜索内容" style="display: none">
                            <i class="iconfont icon-fangdajing sousuo" style="width: 40px;height: 70px;
                            color: yellow;font-size: 32px;text-align: center;margin-top: 20px;"></i>
                            <i class="iconfont icon-shouji shouji" style="width: 40px;height: 70px;
                            color: yellowgreen;font-size: 46px;text-align: center;margin-top: 10px;margin-left: 15px;"></i>
                     <div style="width: 350px;height: 100%;margin-left: 10px;" class="yonghu">
                          <span class="toubox"><img class="tou" src="public/img/tou.png" style="float: left"></span>
                            <div class="hean_left" style="display: block;height: 29px;font-size: 16px;
                            margin-top: 20px;margin-left: 120px;
                            color: white">亲爱的召唤师，欢迎 <em style="color: yellow">登录</em></div>
                            <div class="hean_left" style="display: block;height: 29px;font-size: 12px;
                            margin-left:120px;color: gray">登录后查看自己的战绩、资产、声望值等</div>
                    </div>
                </div>
            </div>
        </div>

		<div class="menu_list">
			<div class="menu_mcd mcd_food ">
                    <div class="food_classic col-lg-8 col-sm-offset-2">
                        <ul class="first">
                            <li><a href="">新手指引</a></li>
                            <li><a href="">资料库</a></li>
                            <li><a href="">攻略中心</a></li>
                            <li><a href="">模式实验室</a></li>
                            <li><a href="">海克斯战利品库</a></li>
                            <li><a href="">屏保下载</a></li>
                            <li><a href="">宇宙官网</a></li>
                        </ul>
                        <ul style="">
                            <li><a href="">点券充值</a></li>
                            <li><a href="">道聚城</a></li>
                            <li><a href="">周边商城</a></li>
                            <li><a href="">LOL桌游</a></li>
                            <li><a href="">LOL信用卡</a></li>
                            <li><a href="">网吧特权</a></li>
                        </ul>
                        <ul style="margin-left: 110px;">
                            <li><a href="">在线客服</a></li>
                            <li><a href="">视频中心</a></li>
                            <li><a href="">活动中心</a></li>
                            <li><a href="">官方论坛</a></li>
                            <li><a href="">官方微信</a></li>
                            <li><a href="">官方微博</a></li>
                        </ul>
                        <ul style="margin-left: 110px;">
                            <li><a href="">全球总决赛</a></li>
                            <li><a href="">LPL职业联赛</a></li>
                            <li><a href="">德玛西亚杯</a></li>
                            <li><a href="">城市争霸赛</a></li>
                            <li><a href="">高校联赛</a></li>
                            <li><a href="">LDL发展联赛</a></li>
                            <li><a href="">高校自发赛</a></li>
                        </ul>
                        <ul style="">
                            <li><a href="">点亮图标</a></li>
                            <li><a href="">声望系统</a></li>
                            <li><a href="">封号查询</a></li>
                            <li><a href="">体验服申请</a></li>
                            <li><a href="">回放系统</a></li>
                            <li><a href="">服务器状态查询</a></li>
                            <li><a href="">裁决之镰</a></li>
                        </ul>
                    </div>
			</div>
        </div>
	</div>
    <div class="headimage">
        <img class="tututu" src="public/img/head.png" alt="" style="margin-top: -2px">
        <img class="tututu1" src="public/img/head1.png" alt="" style="display: none;margin-top: -2px">

    </div>
    
    <div class="Navbar visible-xs">
      <ul style="text-align: center">
          <li class="col-xs-8" style="font-weight: 700">Don't have hahahahhaha</li>
      </ul>
    </div>
    <div style="height: 900px;width: 100%;">




    <!--导航-->
<div style="margin-top: 50px; margin-left: 200px; ">
    <span class="layui-breadcrumb" lay-separator=">">
        <i class="layui-icon">&#xe68e;</i>
        <a href="">英雄联盟首页</a>
        <a href="">游戏资料</a>
        <a><cite>英雄</cite></a>
    </span>
</div>

    <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief" style="margin-top: 50px;margin-left: 200px;">
    <ul class="layui-tab-title">
        <a href="index.php?c=herolist&g=Font"><li class="layui-this">英雄</li></a>
        <a href="index.php?c=zhuangbei&g=Font"><li>物品</li></a>
        <a href="index.php?c=zhsjn&g=Font"><li >召唤师技能</li></a>
        <a href="index.php?c=fuwen&g=Font"><li>符文</li></a>
    </ul>

  <div class="layui-tab-content"></div>
</div>   

<!--单选-->
 <div class="layui-form-item">
    <div class="layui-input-block" style="margin-left: 200px; margin-top: 20px;">
      <input type="radio" name="h_type" value="全部英雄">全部英雄
      <input type="radio" name="h_type" value="战士" checked style="margin-left: 20px">战士
      <input type="radio" name="h_type" value="法师" checked style="margin-left: 20px">法师
      <input type="radio" name="h_type" value="刺客" checked style="margin-left: 20px">刺客
      <input type="radio" name="h_type" value="坦克" checked style="margin-left: 20px">坦克
      <input type="radio" name="h_type" value="射手" checked style="margin-left: 20px">射手
      <input type="radio" name="h_type" value="辅助" checked style="margin-left: 20px">辅助
    </div>
  </div>

<!--数据展示-->
<div style="margin-left: 200px; width: 900px;">
    <td>
    <?php ob_start();
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value['rows'], 'temp');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['temp']->value) {
$_prefixVariable1=ob_get_clean();
echo $_prefixVariable1;?>

        <tr><img src="<?php ob_start();
echo $_smarty_tpl->tpl_vars['temp']->value['default_face'];
$_prefixVariable2=ob_get_clean();
echo $_prefixVariable2;?>
" title="<?php ob_start();
echo $_smarty_tpl->tpl_vars['temp']->value['Hero_name'];
$_prefixVariable3=ob_get_clean();
echo $_prefixVariable3;?>
" class="yx"></tr>
        <?php ob_start();
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
$_prefixVariable4=ob_get_clean();
echo $_prefixVariable4;?>

    </td>
</div>
    

    </div>
</body>

<?php echo '<script'; ?>
>
    $('.container-mcd .item').hover(function() {
        $('.maskline').css({
            'display': 'none'
        });
        $('.menu_mcd').css({
            'display': 'none'
        });
        var index = $(this).data('index');

        $('.maskline').eq(index-1).css({
            'display': 'block'
        });
        $('.menu_list').css({
            'display': 'block'
        });
        $('.menu_mcd').eq(index-1).css({
            'display': 'flex'
        });
    }, function() {
        $('.menu_list').hover(function() {
            // TODO
        }, function() {
            $('.maskline').css({
                'display': 'none'
            });
            $('.menu_list').css({
                'display': 'none'
            });
            $('.menu_mcd').css({
                'display': 'none'
            });
        });

    });;
    $(".sousuo").click(function(){
        $(".sousuo").css({
            'display':'none'
        });
        $(".shouji").css({
            'display':'none'
        })
        $(".yonghu").css({
            'display':'none'
        });
        $(".fadajing").css({
            'display':'block'
        });
    });

    $(".headimage").click(function(){
        $(".sousuo").css({
            'display':'block'
        });
        $(".shouji").css({
            'display':'block'
        })
        $(".yonghu").css({
            'display':'block'
        });
        $(".fadajing").css({
            'display':'none'
        });
    });
    function mobilePhoneSize() {
        $(".tututu").css({
            'display':'none'
        });
        $(".tututu1").css({
            'display':'block'
        });
    }
    function pcSize() {
        $(".tututu").css({
            'display':'block'
        });
        $(".tututu1").css({
            'display':'none'
        });
    }
    function mobilePhoneSize1() {
        $(".Navbar").css({
            'position':'fixed',
            'top':'0px'
        });
    }
    function pcSize1() {
        $(".Navbar").css({
            'position':'relative',
            'top':'0px'
        });
    }
    window.onscroll = function(){
        var t = document.documentElement.scrollTop || document.body.scrollTop;  //获取距离页面顶部的距离
        if(document.body.offsetWidth < 768) return;
        if(t>90){
            mobilePhoneSize();
        }
        else{
            pcSize();
        }
        if(t>329){
            mobilePhoneSize1();
        }
        else{
            pcSize1();
        }
    };

//注意：选项卡 依赖 element 模块，否则无法进行功能性操作
layui.use('element', function(){
  var element = layui.element;
  
  //…
});

<?php echo '</script'; ?>
>
</html><?php }
}
