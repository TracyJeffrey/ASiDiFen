<?php
/* Smarty version 3.1.30, created on 2018-01-03 02:19:05
  from "D:\wamp64\www\ASiDiFen\Application\Font\View\indexx.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a4c3d99057ba3_45886599',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cc07cbd6e49447b01f9e0dfb40a63336f89a0a4a' => 
    array (
      0 => 'D:\\wamp64\\www\\ASiDiFen\\Application\\Font\\View\\indexx.html',
      1 => 1514945943,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a4c3d99057ba3_45886599 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="./public/css/iconfont.css">
    <link rel="stylesheet" href="./public/css/center.css">
	<link rel="stylesheet" href="./public/BootStrap/css/bootstrap.css">
    <link rel="stylesheet" href="./public/layui/css/layui.css">

    <?php echo '<script'; ?>
 src="./public/layui/layui.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="./public/jquery-3.0.0/jquery-3.0.0.js"><?php echo '</script'; ?>
>
</head>
<body>
	<!-- 头部带导航 -->
	<div class="header" >
        <div style="width: 100%;height: 90px;background: black">
            <div class="header_flex col-lg-10 col-lg-offset-1">
                <div class="container-mcd ">
                    <div class="logo"></div>
                    <div class="item hidden-xs" data-index="1">

                        <div class="item_content">
                            <span class="">游戏资料</span>
                            <p>GAME INFO</p>
                        </div>

                    </div>
                    <div class="item hidden-xs" data-index="1">
                        <div class="item_content">
                            <span class="">商城/合作</span>
                            <p>STORE</p>
                        </div>
                    </div>
                    <div class="item hidden-xs" data-index="1">
                        <div class="item_content">
                            <span class="">用户互动</span>
                            <p>COMMUNITY</p>
                        </div>
                    </div>
                    <div class="item hidden-xs" data-index="1">
                        <div class="item_content">
                            <span class="">赛事中心</span>
                            <p>EVENTS</p>
                        </div>
                    </div>
                    <div class="item hidden-xs" data-index="1">
                        <div class="item_content">
                            <span class="">自助系统</span>
                            <p>SYSTEM</p>
                        </div>
                    </div>
                    <input type="text" class="fadajing" placeholder="请输入搜索内容" style="display: none">
                            <i class="iconfont icon-fangdajing sousuo" style="width: 40px;height: 70px;
                            color: #ffdd13;font-size: 40px;text-align: center;margin-top: 20px;"></i>
                            <i class="iconfont icon-shouji1 shouji" style="width: 40px;height: 70px;
                            color:#ffdd13;font-size: 46px;text-align: center;margin-top: 18px;margin-left: 15px;"></i>
                     <div style="width: 350px;height: 100%;margin-left: 10px;" class="yonghu">
                          <span class="toubox"><img class="tou" src="./public/img/tou.png" style="float: left"></span>
                            <div class="hean_left" style="display: block;height: 29px;font-size: 16px;
                            margin-top: 20px;margin-left: 120px;
                            color: white">亲爱的召唤师，欢迎 <em style="color: yellow">登录</em></div>
                            <div class="hean_left" style="display: block;height: 29px;font-size: 12px;
                            margin-left:120px;color: gray">登录后查看自己的战绩、资产、声望值等</div>
                    </div>
                </div>
            </div>
        </div>
        <!--下拉内容-->
		<div class="menu_list">
			<div class="menu_mcd mcd_food ">
                    <div class="food_classic col-lg-8 col-sm-offset-2">
                        <ul class="first">
                            <li><a href="">新手指引</a></li>
                            <li><a href="index.php?c=herolist&g=Font">资料库</a></li>
                            <li><a href="">攻略中心</a></li>
                            <li><a href="">模式实验室</a></li>
                            <li><a href="">海克斯战利品库</a></li>
                            <li><a href="">屏保下载</a></li>
                            <li><a href="">宇宙官网</a></li>
                        </ul>
                        <ul style="">
                            <li><a href="">点券充值</a></li>
                            <li><a href="">道聚城</a></li>
                            <li><a href="">周边商城</a></li>
                            <li><a href="">LOL桌游</a></li>
                            <li><a href="">LOL信用卡</a></li>
                            <li><a href="">网吧特权</a></li>
                        </ul>
                        <ul style="margin-left: 110px;">
                            <li><a href="">在线客服</a></li>
                            <li><a href="">视频中心</a></li>
                            <li><a href="">活动中心</a></li>
                            <li><a href="">官方论坛</a></li>
                            <li><a href="">官方微信</a></li>
                            <li><a href="">官方微博</a></li>
                        </ul>
                        <ul style="margin-left: 110px;">
                            <li><a href="">全球总决赛</a></li>
                            <li><a href="">LPL职业联赛</a></li>
                            <li><a href="">德玛西亚杯</a></li>
                            <li><a href="">城市争霸赛</a></li>
                            <li><a href="">高校联赛</a></li>
                            <li><a href="">LDL发展联赛</a></li>
                            <li><a href="">高校自发赛</a></li>
                        </ul>
                        <ul style="">
                            <li><a href="">点亮图标</a></li>
                            <li><a href="">声望系统</a></li>
                            <li><a href="">封号查询</a></li>
                            <li><a href="">体验服申请</a></li>
                            <li><a href="">回放系统</a></li>
                            <li><a href="">服务器状态查询</a></li>
                            <li><a href="">裁决之镰</a></li>
                        </ul>
                    </div>
			</div>
        </div>
	</div>
    <!--2018图片-->
    <div class="headimage">
        <img class="tututu" src="./public/img/head.png" alt="" style="margin-top: -2px">
        <img class="tututu1" src="./public/img/head1.png" alt="" style="display: none;margin-top: -2px">

    </div>
    <!--中部导航栏-->
    <div class="Navbar">
        <ul class="col-lg-10 col-sm-12 col-sm-12 col-xs-12">
            <li class="col-sm-1">综合资讯</li>
            <li class="col-sm-1">视频中心</li>
            <li class="col-sm-1">赛事中心</li>
            <li class="col-sm-1">活动中心</li>

        </ul>
    </div>

    <!--缩小后的中部导航栏-->
    <!--<div class="Navbar visible-xs">-->
      <!--<ul style="text-align: center">-->
          <!--<li class="col-xs-8" style="font-weight: 700">Don't have hahahahhaha</li>-->
      <!--</ul>-->
    <!--</div>-->

    <!--网页中部内容-->
    <div class="center container-fluid col-sm-12 col-sm-12 col-xs-12" style="background-color:#f7f8f8">
        <div class="center_bg clearfix">
            <div class="center-top ">
                <div class="center-top-son">
                <div class="center-left"></div>
                </div>
                <div id="promoTrigger" class="layui-nav layui-nav" lay-filter="test">
                    <span class="layui-nav-item c1" data-index="1"><a href="">海克斯科技圣诞礼物</a></span>
                    <!--layui-nav-itemed             -->
                    <span class="layui-nav-item c1" data-index="2"><a href="">无限火力雪球大战</a></span>
                    <span class="layui-nav-item c1" data-index="3"><a href="">2017冰雪节</a></span>
                    <span class="layui-nav-item c1" data-index="4"><a href="">竞技场冬季擂台</a></span>
                    <span class="layui-nav-item c1" data-index="5"><a href="">高校联赛狂欢</a></span>
                </div>
            </div>
            <!--右侧下载和功能栏目-->
            <div class="center-right clearfix">
                <video src="./public/video/down.mp4" class="" autoplay loop></video>
                <ul class="center-right-ul">
                    <li class="first_li" data-index="1"><i class="iconfont icon-lubiao-" style="font-size: 32px"></i>新手推荐</li>
                    <li data-index="2"><i class="iconfont icon-yuzhouxingqiu-17" style="font-size: 40px;margin-left: 15px"></i>宇宙官网</li>
                    <li data-index="3"><i class="iconfont icon-cart"></i>周边商城</li>
                    <li class="first_li" data-index="4"><i class="iconfont icon-lol1"></i>新客户端</li>
                    <li data-index="5"><i class="iconfont icon-wangzheqishi"></i>峡谷之巅</li>
                    <li data-index="6"><i class="iconfont icon-yuechi"></i>CDK兑换</li>
                    <li class="first_li" data-index="7"><i class="iconfont icon-lunhuan"></i>轮换模式</li>
                    <li data-index="8"><i class="iconfont icon-shu"></i>游戏资料</li>
                    <li data-index="9"><i class="iconfont icon-lingqujiangli"></i>领取中心</li>
                    <li class="first_li" data-index="10"><i class="iconfont icon-iconfonticonfontweibo"></i>官方微博</li>
                    <li data-index="11"><i class="iconfont icon-luntan"></i>玩家论坛</li>
                    <li data-index="12"><i class="iconfont icon-kefu"></i>在线客服</li>
                </ul>
            </div>
        </div>

        <!--最新资讯-->
        <div class="allnews">
            <div class="nownews">
                <div class="nownewsTitle clearfix">
                    <h2>最新资讯</h2>
                    <ul class="nownewsTitle-ui" style="margin-top: 14px">
                        <li>综合新闻</li>
                        <li>官方公告</li>
                        <li>赛事新闻</li>
                        <li>论坛资讯</li>
                    </ul>
                </div>
                <!--新闻左内容-->
                <div class="nownewsLeft">
                    <ul>
                        <li class="nownewsLeftFirst_li clearbox">
                            <a href="" class="clearbox">
                                <img src="./public/img/new1.jpg" alt="">
                                <span class="news-first-title">12月26日19点30LPL春季赛抽签直播</span>
                                <span class="news-first-desc">2018《英雄联盟》职业联赛(简称：LPL)春季赛抽签分组仪式将于12月26日19:30进行。召唤师粉丝观众们可以通过LOL电视台、掌上英雄联盟、赛事官网(lpl.qq.com)观看抽签直播。同时，2018年LPL春季赛也将1月15日（周一）17：00正式开赛！</span>
                            </a>
                        </li>
                        <li class="news-item">
                            <a href="" class="lnk-tit">
                                <span class="new-type">新闻</span>
                                &nbsp;&nbsp;&nbsp;&nbsp;12月25日第一条圣诞节快乐
                                <span class="date">12/25</span>
                            </a>
                        </li>
                        <li class="news-item">
                            <a href="" class="lnk-tit">
                                <span class="new-type">新闻</span>
                                &nbsp;&nbsp;&nbsp;&nbsp;12月25日第一条圣诞节快乐
                                <span class="date">12/25</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--点击加载更多-->
                <a class="news-more">
                    阅读更多<em>加载更多</em>
                </a>
            </div>
            <!--最新资讯右-->
            <div class="nownews2">
                <!--右标题-->
                <div class="nownewsTitle_right clearfix">
                    <ul class="nownewsTitle-right-ui" style="margin-top: 14px">
                        <li>综合新闻</li>
                        <li>官方公告</li>
                        <li>赛事新闻</li>
                    </ul>
                </div>
                <!--新闻右内容-->
                <div class="nownewsRight clearbox">
                    <div class="dt">
                        <div class="dt_model">
                            <div class="dt_iteam" data-toggle="tooltip" data-placement="bottom" title="最萌精灵">
                                <a href="#">
                                    <img src="./public/img/1.jpg" alt="" class="img">
                                    <div class="mask">
                                        <!--此处为应为数据库调用的-->
                                        <div class="title">最萌精灵</div>
                                    </div>

                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-go">
                        <img src="./public/img/pic-go-qt.jpg" alt="">
                        <img src="./public/img/pic-go-mall.jpg" alt="">
                    </div>
                    <div class="side-tips-18">
                        <i class="iconfont icon-iconlin"></i>
                        <h5>本游戏适合18岁（含）以上玩家娱乐</h5>
                        <p>
                            抵制不良游戏 拒绝盗版游戏 注意自我保护 谨防受骗上当
                            <br>
                            适度游戏益脑 沉迷游戏伤身 合理安排时间 享受健康生活
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!--底部上面的标题-->
        <div class="nownewsBottomTitle clearfix">
            <h2>最新推荐</h2>
            <ul class="nownewsTitle-ui" style="margin-top: 14px">
                <li>视频推荐</li>
                <li>活动推荐</li>
            </ul>
        </div>
        <!--底部上面的标题的内容-->
        <div class="recommend-content">
            <ul class="nownewsBottomTitle_bottom clearbox">
                <li style="position: relative;">
                    <div class="begin_all">
                    <span class="iconfont icon-kaishishijian begin"></span>
                    <span class="iconfont icon-kaishishijian begin1"></span>
                    </div>
                    <div class="playtime">11:22</div>
                    <img src="./public/img/0.jpg" alt="">
                    <h5>主播玩脱了38：经典再现！骚男弟弟同赴死！</h5>
                </li>
            </ul>
            <a href="" class="recommend-more">
                浏览更多
                <em>视频内容</em>
            </a>
        </div>
    </div>
    <!--底部-->
    <div class="footer container-fluid col-sm-12 col-sm-12 col-xs-12">

    </div>

</body>
<?php echo '<script'; ?>
 src="./public/js/bothjs.js"><?php echo '</script'; ?>
>
</html><?php }
}
