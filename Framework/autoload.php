<?php
 function autoload($classname){
    $frame_work_list=array(
        'Controller'=> FRAMEWORK.'Controller.class.php',
        'Model'=> FRAMEWORK.'Model.class.php',
        'MysqlDB'=> FRAMEWORK.'MysqlDB.class.php'
    );
    if(isset($frame_work_list[$classname])){
        require $frame_work_list[$classname];
    }elseif(substr($classname,-10) == 'Controller'){
        require APPLICATION.GROUP.'/controller/'.$classname.'.class.php';
    }elseif(substr($classname,-5)=='Model'){
        require APPLICATION.GROUP.'/model/'.$classname.'.class.php';
    }          
}

spl_autoload_register('autoload');