<?php

class Controller{
    
    private $smarty;
    
    public function __construct() {
        $this->smartyConfig();
    }
    
    /**
     * 配置smarty的选项
     */
    private function smartyConfig(){
        
        require FRAMEWORK."smarty/Smarty.class.php";
    
        $this->smarty = new Smarty();
        
        //设置配置
        $this->smarty->template_dir = APPLICATION.GROUP."/View/";
        $this->smarty->compile_dir = ROOT_PATH.'public/templates_c/';
        $this->smarty->config_dir = ROOT_PATH.'public/configs/';
        $this->smarty->cache_dir = ROOT_PATH.'public/cache/';
    }
    /**
     * 控制器向页面传输数据
     * @param type $name 页面的变量名称
     * @param type $value 页面变量的值
     */
    public function assign($name, $value){
        $this->smarty->assign($name,$value);
    }
    /**
     * 指定页面
     * @param type $view 页面的名称
     */
    public function display($view){
        $this->smarty->display($view);
    }
    /**
     * post方式获取数据
     * @param string 字段名称
     * @return string 返回值
     */
    public static function post($name = ""){
        if($name == ""){
            return $_POST;
        }else{
            return isset($_POST[$name])?$_POST[$name]:"";
        }
        
    }
    /**
     * get方式获取数据
     * @param string 字段名称
     * @return string 返回值
     */
    public static function get($name = ""){
        if($name == ""){
            return $_GET;
        }else{
            return isset($_GET[$name])?$_GET[$name]:"";
        }
        
    }
    public static function redirct($url){
        echo "<script>window.location.href='{$url}';</script>";
    }
}

