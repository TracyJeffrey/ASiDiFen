<!doctype html>
<html lang="en">
<head>
	<title>Tables | Klorofil - Free Bootstrap Dashboard Template</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<script src="jquery-3.0.0/jquery-3.0.0.js"></script>
</head>
<style type="text/css">
	 .title{
		text-align: center;
	 }
	 .fenye{
			text-align: center;
	 }
</style>
<body>
	<div id="wrapper">
	
			<!-- MAIN CONTENT -->
			<div class="main-content1">
				<div style="width: 100%;">
					<div class="row" style="margin: 0 auto;">
							<div class="panel">
								<div class="panel-heading title">
									<h3 class="panel-title">账户管理</h3>
								</div>
								<div class="panel-body">
									<table class="table table-bordered table-striped table-hover">
										<thead>
											<tr>
												<th>序号</th>
												<th>用户名称</th>
												<th>昵称</th>
												<th>性别</th>
												<th>是否封号</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>12</td>
												<td>123</td>
												<td>1233</td>
												<td>1233321</td>
												<td>12323123</td>
												<td><button>重置密码</button></td>
											</tr>
										</tbody>
									</table>
					<div class="fenye">
									<ul class="pagination">
											<li>上一页</li>
											<li>1</li>
											<li>下一页</li>
									</ul>
					</div>		
								</div>
							</div>
							<!-- END BASIC TABLE -->
				
						
					</div>
					
				
				</div>
			</div>
			<!-- END MAIN CONTENT -->
	
		<!-- END MAIN -->
		
	<!-- END WRAPPER -->
	<!-- Javascript -->
	
</body>

</html>
<script type="text/javascript">
		function resetPass(obj){
			var uid =obj.dataset.uid;
			$.post("pwd.php",{uid:uid,},function(data){
				if (data.msg) {
					alert("重置成功");
				}else{
					alert("重置失败");
				}
			},"json");
		}
</script>
