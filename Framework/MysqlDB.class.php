 <?php
/**
 * 操作数据库
 */
class MysqlDB{
    //连接属性
    private $con;
    //错误信息
    private $error="";
    //静态变量
    private static $instance;

    /**
     * 自动连接数据库   config配置 数组 存放 host 主机名
     *                                        name 数据库连接名称
     *                                        pwd 密码
     *                                        dbname 数据库名称
     *                                        chart 数据库连接的编码
     * 
     * $config=array("host"=>,"name"=>,"pwd"=>,"dbname","chart"=>);
     * 
     *  创建对象的时候，调用构造函数
     *  $mysqlDB = new MysqlDB($config);
     */
    private function __construct($config) {
        $this->con = mysqli_connect($config["host"], $config["name"], $config["pwd"], $config["dbname"]);
        if(!$this->con){
            $this->error = mysqli_connect_error();
            return false;
        }
        mysqli_set_charset($this->con, $config["chart"]);
    }
    /**
     * 获取数据库错误信息
     */
    public static function getIntance($config){
        if(self::$instance == false){
            self::$instance = new self($config);
        }
        return self::$instance;
    }
    
    
    public function getError(){
        if($this->error == ""){
            return mysqli_error($this->con);
        }else {
             return $this->error;
        }
       
    }
    /**
     * 选择数据库
     */
    public function selectDB($db){
        $db = ($db==""?$this->dbname:$db);
        if(mysqli_select_db($this->con, $db)){
            return true;
        }else{
            return false;
        }
    }
    /**
     * 执行语句
     */
    public function query($sql){
        if($sql == "") {
            return false;
        }
        $result = mysqli_query($this->con, $sql);
        return $result;
    }
    /**
     * 查询数据多条数据
     */
    public function selectByAll($sql){
        $result = $this->query($sql);
        $arr = array();
        if($result == false) {
            return $arr;
        }
       
        while ($row = mysqli_fetch_assoc($result)){
            $arr[] = $row;
        }
        
        return $arr;
    }
    
    /**
     * 查询单条数据
     */
    public function selectByOne($sql){
        $result = $this->query($sql);
        $arr = array();
        if($result == false) {
            return $arr;
        }
       
        return mysqli_fetch_assoc($result);
    }
    
    /**
     * 获取总条数
     */
    public function count($sql){
        $result = $this->query($sql);
        if($result == false) {
            return 0;
        }
        return mysqli_num_rows($result);
    }
    
    /**
     * 插入数据
     */
    public function insert($sql){
        $result = $this->query($sql);
        if($result == false) {
            return 0;
        }
        return mysqli_insert_id($this->con);
    }
    
    /**
     * 删除数据
     */
    public function delete($sql) {
        $result = $this->query($sql);
        return $result;
    }
    
     /**
     * 修改数据
     */
    public function update($sql) {
        $result = $this->query($sql);
        return $result;
    }
    
    /**
     * 关闭数据库连接
     */
    public function __destruct() {
        mysqli_close($this->con);
    }
}
