<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Apricot 1.3</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <script type="text/javascript" src="Framework/assets/js/jquery.js"></script>

    <link rel="stylesheet" href="Framework/assets/css/style.css">
    <link rel="stylesheet" href="Framework/assets/css/loader-style.css">
    <link rel="stylesheet" href="Framework/assets/css/bootstrap.css">

    <link rel="stylesheet" href="Framework/assets/css/extra-pages.css">






    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="Framework/assets/ico/minus.png">
    <style type="text/css">
        .fenye{
            width: 100%;
            text-align: center;
        }
        .fenye ul{
            text-align: center;
        }
    </style>
</head>

<body><div id="awwwards" class="right black"><a href="http://www.awwwards.com/best-websites/apricot-navigation-admin-dashboard-template" target="_blank">best websites of the world</a></div>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>




                <div id="logo-mobile" class="visible-xs">
                   <h1>后台管理系统<span>wyd</span></h1>
                </div>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">

                    <li class="dropdown">

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i style="font-size:20px;" class="icon-conversation"></i><div class="noft-red">23</div></a>


                        <ul style="margin: 11px 0 0 9px;" role="menu" class="dropdown-menu dropdown-wrap">
                            <li>
                                <a href="#">
                                    <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/men/1.jpg">Jhon Doe <b>Just Now</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/women/1.jpg">Jeniffer <b>3 Min Ago</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/men/2.jpg">Dave <b>2 Hours Ago</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/men/3.jpg"><i>Keanu</i>  <b>1 Day Ago</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/men/4.jpg"><i>Masashi</i>  <b>2 Mounth Ago</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div>See All Messege</div>
                            </li>
                        </ul>
                    </li>
                    <li>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i style="font-size:19px;" class="icon-warning tooltitle"></i><div class="noft-green">5</div></a>
                        <ul style="margin: 12px 0 0 0;" role="menu" class="dropdown-menu dropdown-wrap">
                            <li>
                                <a href="#">
                                    <span style="background:#DF2135" class="noft-icon maki-bus"></span><i>From Station</i>  <b>01B</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span style="background:#AB6DB0" class="noft-icon maki-ferry"></span><i>Departing at</i>  <b>9:00 AM</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span style="background:#FFA200" class="noft-icon maki-aboveground-rail"></span><i>Delay for</i>  <b>09 Min</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span style="background:#86C440" class="noft-icon maki-airport"></span><i>Take of</i>  <b>08:30 AM</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span style="background:#0DB8DF" class="noft-icon maki-bicycle"></span><i>Take of</i>  <b>08:30 AM</b>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div>See All Notification</div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><i data-toggle="tooltip" data-placement="bottom" title="Help" style="font-size:20px;" class="icon-help tooltitle"></i></a>
                    </li>

                </ul>
                <div id="nt-title-container" class="navbar-left running-text visible-lg">
                    <ul class="date-top">
                        <li class="entypo-calendar" style="margin-right:5px"></li>
                        <li id="Date"></li>


                    </ul>

                    <ul id="digital-clock" class="digital">
                        <li class="entypo-clock" style="margin-right:5px"></li>
                        <li class="hour"></li>
                        <li>:</li>
                        <li class="min"></li>
                        <li>:</li>
                        <li class="sec"></li>
                        <li class="meridiem"></li>
                    </ul>
                    <ul id="nt-title">
                        <li><i class="wi-day-lightning"></i>&#160;&#160;Berlin&#160;
                            <b>85</b><i class="wi-fahrenheit"></i>&#160;; 15km/h
                        </li>
                        <li><i class="wi-day-lightning"></i>&#160;&#160;Yogyakarta&#160;
                            <b>85</b><i class="wi-fahrenheit"></i>&#160;; Tonight- 72 °F (22.2 °C)
                        </li>

                        <li><i class="wi-day-lightning"></i>&#160;&#160;Sttugart&#160;
                            <b>85</b><i class="wi-fahrenheit"></i>&#160;; 15km/h
                        </li>

                        <li><i class="wi-day-lightning"></i>&#160;&#160;Muchen&#160;
                            <b>85</b><i class="wi-fahrenheit"></i>&#160;; 15km/h
                        </li>

                        <li><i class="wi-day-lightning"></i>&#160;&#160;Frankurt&#160;
                            <b>85</b><i class="wi-fahrenheit"></i>&#160;; 15km/h
                        </li>

                    </ul>
                </div>

                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">Hi, Dave Mattew <b class="caret"></b>
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="http://themeforest.net/item/apricot-navigation-admin-dashboard-template/7664475?WT.ac=category_item&WT.z_author=themesmile">
                                    <span class="entypo-basket"></span>&#160;&#160; Purchase</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="icon-gear"></span>&#160;&#160;Setting</a>
                        <ul role="menu" class="dropdown-setting dropdown-menu">

                            <li class="theme-bg">
                                <div id="button-bg"></div>
                                <div id="button-bg2"></div>
                                <div id="button-bg3"></div>
                                <div id="button-bg5"></div>
                                <div id="button-bg6"></div>
                                <div id="button-bg7"></div>
                                <div id="button-bg8"></div>
                                <div id="button-bg9"></div>
                                <div id="button-bg10"></div>
                                <div id="button-bg11"></div>
                                <div id="button-bg12"></div>
                                <div id="button-bg13"></div>
                            </li>
                        </ul>
                    </li>
                    <li class="hidden-xs">
                        <a class="toggle-left" href="#">
                            <span style="font-size:20px;" class="entypo-list-add"></span>
                        </a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
        <div id="logo">
         <h1>Apricot<span>v1.3</span></h1>
        </div>

        <a id="toggle">
            <span class="entypo-menu"></span>
        </a>
        <div class="dark">
            <form action="#">
                <span>
                    <input type="text" name="search" value="" class="search rounded id_search" placeholder="操作搜索..." autofocus="">
                </span>
            </form>
        </div>

        <div class="search-hover">
            <form id="demo-2">
                <input type="search" placeholder="Search Menu..." class="id_search">
            </form>
        </div>

            <div class="skin-part">
 <div id="tree-wrap">
                <div class="side-bar">
                    <ul class="topnav menu-left-nest">
                        <li>
                            <a class="tooltip-tip ajax-load" href="./index.php?g=Back&c=user" title="用户管理">
                                <i class="icon-document-edit"></i>
                                <span>用户管理</span>

                            </a>
                        </li>
                        <li>
                            <a class="tooltip-tip ajax-load" href="./index.php?g=Back&c=herolist" title="英雄列表">
                                <i class="icon-feed"></i>
                                <span>英雄列表</span>

                            </a>
                        </li>
                        <li>
                            <a class="tooltip-tip ajax-load" href="./index.php?g=Back&c=heroskin" title="英雄皮肤">
                                <i class="icon-camera"></i>
                                <span>英雄皮肤</span>

                            </a>
                            <li>
                            <a class="tooltip-tip ajax-load" href="./index.php?g=Back&c=jineng" title="英雄技能">
                                <i class="icon-window"></i>
                                <span>英雄技能</span>

                            </a>
                        </li>
                        <li>
                            <a class="tooltip-tip ajax-load" href="./index.php?g=Back&c=jineng1" title="英雄技能">
                                <i class="icon-document"></i>
                                <span>召唤师技能</span>

                            </a>
                        </li>
                        </li>
                    </ul>

                    <ul class="topnav menu-left-nest">                        
                        <li>
                            <a class="tooltip-tip ajax-load" href="./index.php?g=Back&c=zhuangbei" title="装备大全">
                                <i class="icon-mail"></i>
                                <span>装备大全</span>                                
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip ajax-load" href="./index.php?g=Back&c=tianfu" title="天赋大全">
                                <i class="icon-preview"></i>
                                <span>天赋大全</span>
                            </a>
                        </li>

                       


                    </ul>

                    <ul id="menu-showhide" class="topnav menu-left-nest">
                       <li>
                            <a class="tooltip-tip" href="#" title="游戏攻略">
                                <i class="icon-monitor"></i>
                                <span>游戏攻略</span>
                            </a>
                            <ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="./index.php?g=Back&c=jinengtuijian" title="英雄技能推荐"><i class="icon-attachment"></i><span>英雄技能推荐</span></a>
                                </li>
                                <li><a class="tooltip-tip2 ajax-load" href="./index.php?g=Back&c=jinengtj" title="召唤师技能推荐"><i class="icon-view-list-large"></i><span>召唤师技能推荐</span></a>
                                </li>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="./index.php?g=Back&c=tianfutuijian" title="推荐天赋"><i class="icon-folder"></i><span>推荐天赋</span></a>
                                </li>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="./index.php?g=Back&c=tuijianzhuangbei" title="推荐装备"><i class="icon-calendar"></i><span>推荐装备</span></a>
                                </li>
                               
                            </ul>
                        </li>
                        <li>
                            <a class="tooltip-tip" href="./index.php?g=Back&c=kezhi" title="英雄相克表">
                                <i class="icon-document"></i>
                                <span>英雄相克表</span>
                            </a>
                        </li>
                       
                    </ul>

                   
                </div>
            </div>
        </div>
    </div>
    <!-- END OF SIDE MENU -->



    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="fontawesome-money"></span>
                            <span>管理员
                            </span>
                        </h2>

                    </div>


                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        
                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">管理</a>
                </li>
                <li class="pull-right">
                                        <form  action="./index.php" class="form-horizontal" method="get" role="form" >
                                            <label for="firstname" class="col-sm-2 control-label">天赋名称</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="name" class="form-control" id="firstname" placeholder="请输入天赋名字">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">搜索</button>
                                            </div>

                                            <input type="hidden" name="g" value="Back"/>
                                            <input type="hidden" name="c" value="tianfu"/>
                                            <input type="hidden" name="a" value="selectObjects"/>
                                        </form>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->

    <div id="wrapper">
    
            <!-- MAIN CONTENT -->
            <div class="main-content1">
                <div style="width: 100%;">
                    <div class="row" style="margin: 0 auto;">
                            <div class="panel">
                                <div class="panel-heading title">
                                    <h3 class="panel-title">天赋大全</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>天赋名称</th>
                                                <th>天赋效果</th>
                                                <th>天赋图片</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{foreach $list['rows'] as $temp}}
                                            <tr>
                                                <td>{{$temp['id']}}</td>
                                                <td>{{$temp['I_Name']}}</</td>
                                                <td>{{$temp['I_content']}}</</td>
                                                <td><img src="{{$temp['I_img']}}"></td>
                                            </tr>
                                            {{/foreach}}
                                        </tbody>
                                    </table>
                    <div class="fenye">
   {if $list["name"]==null }
                                    <!-- 分页-->
                                    <ul class="pagination">
                                        {if $list["page"] > 1}
                                        <li><a href="./index.php?g=Back&c=tianfu&a=listObjects&page={$list['page']-1}">上一页</a></li>
                                        {/if}

                                        {for $pg=$list["start"] to $list["end"]}
                                        {if $pg==$list["page"]}
                                        <li class='active'><a href='./index.php?g=Back&c=tianfu&a=listObjects&page={$pg}'>{$pg}</a></li>
                                        {else}
                                        <li><a href='./index.php?g=Back&c=tianfu&a=listObjects&page={$pg}'>{$pg}</a></li>
                                        {/if}
                                        {/for}

                                        {if $list["page"] < $list["pages"]}
                                        <li><a href="./index.php?g=Back&c=tianfu&a=listObjects&page={$list['page']+1}">下一页</a></li>
                                        {/if}
                                    </ul>

                                    {else}

                                    <ul class="pagination">
                                        {if $list["page"] > 1}
                                        <li><a href="./index.php?g=Back&c=tianfu&a=selectObjects&page={$list['page']-1}&name={$list['name']}">上一页</a></li>
                                        {/if}

                                        {for $pg=$list["start"] to $list["end"]}
                                        {if $pg==$list["page"]}
                                        <li class='active'><a href='./index.php?g=Back&c=tianfu&a=selectObjects&page={$pg}&name={$list["name"]}'>{$pg}</a></li>
                                        {else}
                                        <li><a href='./index.php?g=Back&c=tianfu&a=selectObjects&page={$pg}&name={$list["name"]}'>{$pg}</a></li>
                                        {/if}
                                        {/for}

                                        {if $list["page"] < $list["pages"]}
                                        <li><a href="./index.php?g=Back&c=tianfu&a=selectObjects&page={$list['page']+1}&name={$list['name']}">下一页</a></li>
                                        {/if}
                                    </ul>
                                    {/if} 
                    </div>      
                                </div>
                            </div>
                            <!-- END BASIC TABLE -->
                
                        
                    </div>
                    
                
                </div>
            </div>
            <!-- END MAIN CONTENT -->








            <!-- /END OF CONTENT -->



            <!-- FOOTER -->
            <div class="footer-space"></div>
            <div id="footer">
                <div class="devider-footer-left"></div>
                <div class="time">
                    <p id="spanDate">
                    <p id="clock">
                </div>
                <div class="copyright">Make with Love
                    <span class="entypo-heart"></span>Collect from <a href="http://www.cssmoban.com/" title="网页模板" target="_blank">网页模板</a> All Rights Reserved</div>
                <div class="devider-footer"></div>

            </div>
            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
    <div class="sb-slidebar sb-right">
        <div class="right-wrapper">
            <div class="row">
                <h3>
                    <span><i class="entypo-gauge"></i>&nbsp;&nbsp;MAIN WIDGET</span>
                </h3>
                <div class="col-sm-12">

                    <div class="widget-knob">
                        <span class="chart" style="position:relative" data-percent="86">
                            <span class="percent"></span>
                        </span>
                    </div>
                    <div class="widget-def">
                        <b>Distance traveled</b>
                        <br>
                        <i>86% to the check point</i>
                    </div>

                    <div class="widget-knob">
                        <span class="speed-car" style="position:relative" data-percent="60">
                            <span class="percent2"></span>
                        </span>
                    </div>
                    <div class="widget-def">
                        <b>The average speed</b>
                        <br>
                        <i>30KM/h avarage speed</i>
                    </div>


                    <div class="widget-knob">
                        <span class="overall" style="position:relative" data-percent="25">
                            <span class="percent3"></span>
                        </span>
                    </div>
                    <div class="widget-def">
                        <b>Overall result</b>
                        <br>
                        <i>30KM/h avarage Result</i>
                    </div>
                </div>
            </div>
        </div>

        <div style="margin-top:0;" class="right-wrapper">
            <div class="row">
                <h3>
                    <span><i class="entypo-chat"></i>&nbsp;&nbsp;CHAT</span>
                </h3>
                <div class="col-sm-12">
                    <span class="label label-warning label-chat">Online</span>
                    <ul class="chat">
                        <li>
                            <a href="#">
                                <span>
                                    <img alt="" class="img-chat img-circle" src="http://api.randomuser.me/portraits/thumb/men/20.jpg">
                                </span><b>Dave Junior</b>
                                <br><i>Last seen : 08:00 PM</i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <img alt="" class="img-chat img-circle" src="http://api.randomuser.me/portraits/thumb/men/21.jpg">
                                </span><b>Kenneth Lucas</b>
                                <br><i>Last seen : 07:21 PM</i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <img alt="" class="img-chat img-circle" src="http://api.randomuser.me/portraits/thumb/men/22.jpg">
                                </span><b>Heidi Perez</b>
                                <br><i>Last seen : 05:43 PM</i>
                            </a>
                        </li>


                    </ul>

                    <span class="label label-chat">Offline</span>
                    <ul class="chat">
                        <li>
                            <a href="#">
                                <span>
                                    <img alt="" class="img-chat img-offline img-circle" src="http://api.randomuser.me/portraits/thumb/men/23.jpg">
                                </span><b>Dave Junior</b>
                                <br><i>Last seen : 08:00 PM</i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <img alt="" class="img-chat img-offline img-circle" src="http://api.randomuser.me/portraits/thumb/women/24.jpg">
                                </span><b>Kenneth Lucas</b>
                                <br><i>Last seen : 07:21 PM</i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <img alt="" class="img-chat img-offline img-circle" src="http://api.randomuser.me/portraits/thumb/men/25.jpg">
                                </span><b>Heidi Perez</b>
                                <br><i>Last seen : 05:43 PM</i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <img alt="" class="img-chat img-offline img-circle" src="http://api.randomuser.me/portraits/thumb/women/25.jpg">
                                </span><b>Kenneth Lucas</b>
                                <br><i>Last seen : 07:21 PM</i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <img alt="" class="img-chat img-offline img-circle" src="http://api.randomuser.me/portraits/thumb/men/26.jpg">
                                </span><b>Heidi Perez</b>
                                <br><i>Last seen : 05:43 PM</i>
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- END OF RIGHT SLIDER CONTENT-->




    <!-- MAIN EFFECT -->
    <script type="text/javascript" src="Framework/assets/js/preloader.js"></script>
    <script type="text/javascript" src="Framework/assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="Framework/assets/js/app.js"></script>
    <script type="text/javascript" src="Framework/assets/js/load.js"></script>
    <script type="text/javascript" src="Framework/assets/js/main.js"></script>


</body>

</html>
