<?php

class userController extends Controller {
    
    public function listObjects(){
         $userModel = new userModel();       
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $userModel->selectObjects($page, 3);
         //使用smarty技术
         $this->assign("list", $list);
         
         $this->display("user/user.tpl");         
    }
    
     public function selectObjects(){
         $userModel = new userModel();
         
         $name = $this->get("name");
         
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $userModel->selectWhereObjects($page, 3,$name);
         //使用smarty技术
         $this->assign("list", $list);       
         $this->display("user/user.tpl");        
    }   
      /**
       * 删除操作的实现
       */

}
