<?php

class jineng1Controller extends Controller {
    
    public function listObjects(){
         $jineng1Model = new jineng1Model();       
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $jineng1Model->selectObjects($page, 3);
         //使用smarty技术
         $this->assign("list", $list);
         
         $this->display("skill/jineng1.tpl");         
    }
    
     public function selectObjects(){
         $jineng1Model = new jineng1Model();
         
         $name = $this->get("name");
         
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $jineng1Model->selectWhereObjects($page, 3,$name);
         //使用smarty技术
         $this->assign("list", $list);       
         $this->display("skill/jineng1.tpl");        
    }   
      /**
       * 删除操作的实现
       */

}