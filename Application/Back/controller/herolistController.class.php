<?php

class herolistController extends Controller {
    
    public function listObjects(){
         $herolistModel = new herolistModel();       
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $herolistModel->selectObjects($page, 3);
         //使用smarty技术
         $this->assign("list", $list);
         
         $this->display("hero/heroinfo.tpl");         
    }
    
     public function selectObjects(){
         $herolistModel = new herolistModel();
         
         $name = $this->get("name");
         
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $herolistModel->selectWhereObjects($page, 3,$name);
         //使用smarty技术
         $this->assign("list", $list);       
         $this->display("hero/heroinfo.tpl");        
    }   
      /**
       * 删除操作的实现
       */

}
