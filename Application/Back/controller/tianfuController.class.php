<?php

class tianfuController extends Controller {
    
    public function listObjects(){
         $tianfuModel = new tianfuModel();       
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $tianfuModel->selectObjects($page, 3);
         //使用smarty技术
         $this->assign("list", $list);
         
         $this->display("tianfu/tianfu.tpl");         
    }
    
     public function selectObjects(){
         $tianfuModel = new tianfuModel();
         
         $name = $this->get("name");
         
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $tianfuModel->selectWhereObjects($page, 3,$name);
         //使用smarty技术
         $this->assign("list", $list);       
         $this->display("tianfu/tianfu.tpl");        
    }   
      /**
       * 删除操作的实现
       */

}
