<?php

class jinengController extends Controller {
    
    public function listObjects(){
         $jinengModel = new jinengModel();       
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $jinengModel->selectObjects($page, 3);
         //使用smarty技术
         $this->assign("list", $list);
         
         $this->display("skill/jineng.tpl");         
    }
    
     public function selectObjects(){
         $jinengModel = new jinengModel();
         
         $name = $this->get("name");
         
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $jinengModel->selectWhereObjects($page, 3,$name);
         //使用smarty技术
         $this->assign("list", $list);       
         $this->display("skill/jineng.tpl");        
    }   
      /**
       * 删除操作的实现
       */

}