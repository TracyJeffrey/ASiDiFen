<?php

class loginController extends Controller {
    
    public function listObjects(){
         $loginModel = new loginModel();       
        
         //返回的是当前页的数据
         $list = $loginModel->selectObjects();
         //使用smarty技术
         $this->assign("list", $list);         
         $this->display("login.php");         
    }
}
