<?php

class heroskinController extends Controller {
    
    public function listObjects(){
         $heroskinModel = new heroskinModel();       
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $heroskinModel->selectObjects($page, 3);
         //使用smarty技术
         $this->assign("list", $list);
         
         $this->display("hero/heroskin.tpl");         
    }
    
     public function selectObjects(){
         $heroskinModel = new heroskinModel();
         
         $name = $this->get("name");
         
         $page=$this->get("page");
        
         if($page==""){
             $page=1;
         }
         //返回的是当前页的数据
         $list = $heroskinModel->selectWhereObjects($page, 3,$name);
         //使用smarty技术
         $this->assign("list", $list);       
         $this->display("hero/heroskin.tpl");        
    }   
      /**
       * 删除操作的实现
       */

}
