<?php

// require '../../../Framework/Model.class.php';

class userModel extends Model {

    /**
     * 查询分页数据
     */
    public function selectObjects($nowPage, $pageSize) {
        
         //查询总记录数
        $sqlcount = "select count(*) as total from user";
         //执行查询单行记录
        $row=$this->link->selectByOne($sqlcount);
         //获取总记录数
        $total =$row["total"];
        
         //计算总页数
         //计算总页数
        $pages = ceil($total / $pageSize);
        
        //判断如果当前页大于了总页数，让当前页为总页数
        if($nowPage>$pages){
            $nowPage=$pages;
        }
        
        //返回的数据
        $list = array();

        //定义sql语句
        $sql = "select * from user limit " . ($nowPage - 1) * $pageSize . "," . $pageSize;
        //查询操作 当前页显示的数据
        $rows = $this->link->selectByAll($sql);

        //网页中有开始页码
        $startPage = $nowPage - 2;
       //网页中有结束页码
        $endPage = $nowPage + 2;
        //网页中显示的页码数量
        $showPages = 5;

        //开始显示的页码计算
        if($startPage<1){
            $startPage=1;
            $endPage=$showPages;
        }

        //从结束的页码计算
        if($endPage>$pages){
            $endPage=$pages;
            $startPage=($endPage-$showPages+1)>0?($endPage-$showPages+1):1;
        }

        //存储到数组中
        $list["page"]=$nowPage;
        $list["pages"]=$pages;
        $list["rows"]=$rows;
        $list["start"]=$startPage;
        $list["end"]=$endPage;
        $list["total"]=$total;
        
        
        
        //定义查询的条件字段对应的value值
        $list["name"]=null;
        
        
        
        return $list;
    }
    
    
    
     /**
     * 查询分页数据
     */
    public function selectWhereObjects($nowPage, $pageSize,$name) {
        
          //查询总记录数
         $sqlcount = "select count(*) as total from user where user_name like '%{$name}%'";
         //执行查询单行记录
         $row=$this->link->selectByOne($sqlcount);
         //获取总记录数
         $total =$row["total"];
        
         //计算总页数
         //计算总页数
        $pages = ceil($total / $pageSize);
        
        
        if($nowPage>$pages){
            $nowPage=$pages;
        }
        
        //返回的数据
        $list = array();

        //定义sql语句
        $sql = "select * from user where user_name like '%{$name}%'  limit " . ($nowPage - 1) * $pageSize . "," . $pageSize;
        //查询操作 当前页显示的数据
        $rows = $this->link->selectByAll($sql);


        
      

        //网页中有开始页码
        $startPage = $nowPage - 2;
       //网页中有结束页码
        $endPage = $nowPage + 2;
        //网页中显示的页码数量
        $showPages = 5;

        //开始显示的页码计算
        if($startPage<1){
            $startPage=1;
            $endPage=$showPages;
        }

        //从结束的页码计算
        if($endPage>$pages){
            $endPage=$pages;
            $startPage=($endPage-$showPages+1)>0?($endPage-$showPages+1):1;
        }

        //存储到数组中
        $list["page"]=$nowPage;
        $list["pages"]=$pages;
        $list["rows"]=$rows;
        $list["start"]=$startPage;
        $list["end"]=$endPage;
        $list["total"]=$total;
        
        //定义查询的条件字段对应的value值
        $list["name"]=$name;
        
        return $list;
    }

    /**
     * 根据id查询数据
     */
    public function selectObjectById($id) {
        //定义sql语句
        $sql = "select * from user where id=" . $id;
        //查询操作
        return $this->link->selectByOne($sql);
    }
    
    
    
    /**
     * 删除操作的实现
     * @param type $id
     * @return type
     */
    public function  deleteObjectById($id){
        //定义sql语句
        $sql="delete from user where id=".$id;
        //执行数据库操作
        return $this->link->delete($sql);
    }

    public function  updateObjectById($id){
        //定义sql语句
        $sql="update user SET user_pwd=12321 where id=".$id;
        //执行数据库操作
        return $this->link->delete($sql);
    }
}


// $herolistModel = new herolistModel();

// $herolist = $herolistModel->selectObjects(1, 3);

// var_dump($herolist);

