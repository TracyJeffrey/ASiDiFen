<?php

class herolistController extends Controller {
    
    public function listObjects(){
         $herolistModel = new herolistModel();       

         //返回的是当前页的数据
         $list = $herolistModel->selectObjects();
         //使用smarty技术
         $this->assign("list", $list);
         
         $this->display("yx.html");         
    }
}