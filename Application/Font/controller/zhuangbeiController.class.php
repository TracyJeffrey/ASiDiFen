<?php

class zhuangbeiController extends Controller {
    
    public function listObjects(){
         $zhuangbeiModel = new zhuangbeiModel();       

         //返回的是当前页的数据
         $list = $zhuangbeiModel->selectObjects();
         //使用smarty技术
         $this->assign("list", $list);
         
         $this->display("wp.html");         
    }
}