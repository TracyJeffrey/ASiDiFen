<?php
/**
 * 后台的单入口文件
 */
//定义基础的目录全局常量
define("ROOT_PATH", getcwd()."/");
define("APPLICATION", ROOT_PATH."Application/");
define("FRAMEWORK", ROOT_PATH."Framework/");


//获取传值参数
$controller = isset($_GET["c"])?$_GET["c"]:"index";  // $controller = user
$action = isset($_GET["a"])?$_GET["a"]:"listObjects";  //$action = listObjects

$group = isset($_GET["g"])?$_GET["g"]:"Font";  //$action = listObjects


define("CONTROLLER", $controller); ///user
define("ACTION", $action);  // listObjects
define("GROUP", $group);


//引入自动加载文件
require_once FRAMEWORK."autoload.php";
//定义真正的controller的类的名称   // userController
$controllerName = $controller."Controller";
echo $controllerName;
$controller = new $controllerName();
$controller->$action();

